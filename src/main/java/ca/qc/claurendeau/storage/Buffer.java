package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer {

    private Element first;
    private Element last;
    private int capacity;
    private static int currentLoad;

    public Buffer(int capacity) {
        currentLoad = 0;
        this.capacity = capacity;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Element current = first;
        while (current != null) {
            stringBuilder.append(current.getData()).append(" ");
            current = current.getNext();
        }
        return stringBuilder.toString();
    }

    public int capacity() {
        return capacity;
    }

    public int getCurrentLoad() {
        return currentLoad;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public boolean isFull() {
        return currentLoad == capacity;
    }

    public synchronized void addElement(Element element) throws BufferFullException {

        if (isFull())
            throw new BufferFullException();

        Element current = last;
        last = new Element();
        last = element;

        if (isEmpty())
            first = last;
        else
            current.setNext(last);
        ++currentLoad;
    }

    public synchronized Element removeElement() throws BufferEmptyException {

        if (isEmpty())
            throw new BufferEmptyException();

        Element removedElement = first;
        first = first.getNext();
        if (--currentLoad == 0)
            last = null;
        return removedElement;
    }
}
