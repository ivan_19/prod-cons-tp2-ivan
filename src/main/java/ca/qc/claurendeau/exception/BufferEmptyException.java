package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

    private final static String MESSAGE = " Buffer is empty";

    public BufferEmptyException() {
        super(MESSAGE);
    }
}
