package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {

    private final static String MESSAGE = " Buffer is full";

    public BufferFullException() {
        super(MESSAGE);
    }
}
