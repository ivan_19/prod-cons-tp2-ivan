package ca.qc.claurendeau.storage;

import static org.assertj.core.api.Assertions.*;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BufferTest {

    private Buffer buffer;
    private Element element;
    private int capacity;
    private int data;

    @Before
    public void setUp() throws Exception {
        capacity = 8;
        data = 10;
        buffer = new Buffer(capacity);
        element = new Element();
        element.setData(data);
    }

    @Test
    public void testCapacityBuffer() {
        assertThat(buffer.capacity()).as("Capacity Buffer").isEqualTo(capacity);
    }

    @Test
    public void testBufferIsEmpty() {
        assertThat(buffer.isEmpty()).as("Empty Buffer").isTrue();
    }

    @Test
    public void shouldAddElementTest() throws BufferFullException {
        buffer.addElement(element);
        assertThat(buffer.isEmpty()).isFalse();
    }

    @Test
    public void shouldRemoveElementTest() throws BufferFullException, BufferEmptyException {
        fillBuffer();
        for (int i = 0; i < capacity; i++)
            buffer.removeElement();

        assertThat(buffer.isEmpty()).isTrue();
    }

    @Test
    public void testGetCurrentLoad() throws BufferFullException {
        for (int i = 1; i < capacity; i++) {
            element = constuctElement(i);
            buffer.addElement(element);
            assertThat(buffer.getCurrentLoad()).as("Number elements").isEqualTo(i);
        }
    }

    @Test
    public void testBufferIsFull() throws BufferFullException {
        for (int i = 1; i <= capacity; i++) {
            element = constuctElement(i);
            buffer.addElement(element);
        }
        assertThat(buffer.isFull()).as("Buffer is full").isTrue();
    }

    @Test(expected = BufferFullException.class)
    public void shouldThrowBufferFullExceptionTest() throws BufferFullException {
        for (int i = 0; i <= 12; i++) {
            element = constuctElement(i);
            buffer.addElement(element);
        }
        assertThat(buffer.getCurrentLoad()).isEqualTo(capacity);
    }

    @Test(expected = BufferEmptyException.class)
    public void shouldThrowBufferEmptyExceptionTest() throws BufferFullException, BufferEmptyException {
        int noElement = 0;
        fillBuffer();
        for (int i = 0; i <= capacity; i++)
            buffer.removeElement();

        assertThat(buffer.getCurrentLoad()).isEqualTo(noElement);
    }

    @Test
    public void testToString() throws BufferFullException {
        for (int i = 0; i <= capacity; i++) {
            element = constuctElement(i);
            buffer.addElement(element);
            assertThat(buffer.toString()).startsWith(String.valueOf(0))
                                            .contains(String.valueOf(i))
                                            .doesNotContain(String.valueOf(++i));
        }
        assertThat(buffer.toString()).endsWith(capacity + " ");
    }

    private void fillBuffer() throws BufferFullException {
        for (int i = 0; i < capacity; i++) {
            element = constuctElement(i);
            buffer.addElement(element);
        }
    }

    private Element constuctElement(int data) {
        element = new Element();
        element.setData(data);
        return element;
    }

    @After
    public void tearDown() throws Exception {
        buffer = null;
        element = null;
    }
}


